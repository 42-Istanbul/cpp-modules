/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   File.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/26 21:39:23 by akarahan          #+#    #+#             */
/*   Updated: 2022/04/26 22:38:05 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILE_H
# define FILE_H

# include <iostream>
# include <fstream>
# include <string>

class File
{
	private:
		std::ifstream	inFile;
		std::ofstream	outFile;
		std::string		text;

	public:
		File(char *fileName);
		~File();
		void	readFile(void);
		void	writeFile(void);
		void	replace(std::string s1, std::string s2);
};

#endif
