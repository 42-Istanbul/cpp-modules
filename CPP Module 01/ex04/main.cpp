/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/26 21:30:38 by akarahan          #+#    #+#             */
/*   Updated: 2022/04/26 23:00:58 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "File.hpp"

int	main(int ac, char **av)
{
	if (ac == 4)
	{
		File	file(av[1]);
		file.readFile();
		file.replace(std::string(av[2]), std::string(av[3]));
		file.writeFile();
	}
	return (0);
}
