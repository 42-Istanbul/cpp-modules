/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   File.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/26 21:40:27 by akarahan          #+#    #+#             */
/*   Updated: 2022/04/26 23:02:38 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "File.hpp"

File::File(char *fileName)
{
	std::cout << "Opening the file..." << std::endl;
	inFile.open(fileName);
	if (!inFile.is_open())
		std::cout << "Unable to open file" << std::endl;
	std::string	localFile(fileName);
	localFile.append(".replace");
	outFile.open(localFile.c_str());
	if (!outFile.is_open())
		std::cout << "Unable to create file" << std::endl;
}

File::~File()
{
	std::cout << "Closing the file..." << std::endl;
	inFile.close();
	outFile.close();
}

void	File::readFile(void)
{
	std::string	str;

	while (std::getline(this->inFile, str))
	{
		text.append(str);
		if (!inFile.eof())
			text.append("\n");
	}
}

void	File::writeFile(void)
{
	outFile << text;
}

void	File::replace(std::string s1, std::string s2)
{
	size_t	idx;
	while ((idx = text.find(s1)) != std::string::npos)
	{
		text.erase(idx, s1.length());
		text.insert(idx, s2);
	}
}
