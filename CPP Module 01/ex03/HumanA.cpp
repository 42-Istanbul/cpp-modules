/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/26 18:07:00 by akarahan          #+#    #+#             */
/*   Updated: 2022/04/26 21:28:25 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"

HumanA::HumanA(std::string nameIn, Weapon &weaponIn) : name(nameIn), weapon(weaponIn) {}

HumanA::~HumanA() {}

void	HumanA::attack(void) const
{
	std::cout << this->name << " attacks with their " << this->weapon.getType();
	std::cout << std::endl;
}
