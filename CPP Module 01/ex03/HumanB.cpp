/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/26 18:07:55 by akarahan          #+#    #+#             */
/*   Updated: 2022/04/26 21:32:36 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"

HumanB::HumanB(std::string nameIn) : name(nameIn) {}

HumanB::~HumanB() {}

void	HumanB::attack(void) const
{
	if (this->weapon)
		std::cout << this->name << " attacks with their " << (*this->weapon).getType();
	else
		std::cout << this->name << " has no weapon";
	std::cout << std::endl;
}

void	HumanB::setWeapon(Weapon weaponIn)
{
	(*this->weapon).setType(weaponIn.getType());
}
