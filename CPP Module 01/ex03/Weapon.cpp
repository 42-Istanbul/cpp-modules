/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/26 18:05:01 by akarahan          #+#    #+#             */
/*   Updated: 2022/04/26 21:25:58 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"

Weapon::Weapon(std::string typeIn) : type(typeIn) {}

Weapon::~Weapon() {}

std::string	Weapon::getType(void) const
{
	return (this->type);
}

void	Weapon::setType(std::string newType)
{
	this->type = newType;
}
