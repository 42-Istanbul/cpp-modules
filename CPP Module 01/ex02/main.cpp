/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/26 18:03:50 by akarahan          #+#    #+#             */
/*   Updated: 2022/04/26 18:03:51 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <iomanip>

int	main(void)
{
	std::string	brian = "HI THIS IS BRAIN";
	std::string	*stringPTR = &brian;
	std::string	&stringREF = brian;

	std::cout << std::setw(20) << "string address: " << &brian << std::endl;
	std::cout << std::setw(20) << "stringPTR address: " << &stringPTR << std::endl;
	std::cout << std::setw(20) << "stringREF address: " << &stringREF << std::endl;
	std::cout << std::endl;
	std::cout << std::setw(15) << "string value: " << brian << std::endl;
	std::cout << std::setw(15) << "string value: " << *stringPTR << std::endl;
	std::cout << std::setw(15) << "string value: " << stringREF << std::endl;
	return (0);
}
