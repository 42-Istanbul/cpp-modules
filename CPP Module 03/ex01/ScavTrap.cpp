/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 11:19:23 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/01 11:56:04 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(std::string name) : ClapTrap(name)
{
	this->_hitPts = 100;
	this->_energyPts = 50;
	this->_attackDmg = 20;
	std::cout << "ScavTrap: Default constructor is called" << std::endl;
}

ScavTrap::~ScavTrap()
{
	std::cout << "ScavTrap: Destructor is called" << std::endl;
}

void	ScavTrap::attack(const std::string& target)
{
	if ((int)(this->_energyPts - 1) <= 0)
	{
		this->_energyPts = 0;
		std::cout << "ScavTrap " << this->_name;
		std::cout << "don't have enough energy to attack" << std::endl;
	}
	else
	{
		--this->_energyPts;	
		std::cout << "ScavTrap " << this->_name << " attacks " << target;
		std::cout << ", causing " << this->_attackDmg << " points of damage!" << std::endl;
	}
}

void	ScavTrap::guardGate(void)
{
	std::cout << "ScavTrap " << this->_name << " is in Gate keeper mode" << std::endl;
}
