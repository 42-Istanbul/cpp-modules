/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 11:48:37 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/01 11:58:00 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(std::string name) : ClapTrap(name)
{
	this->_hitPts = 100;
	this->_energyPts = 100;
	this->_attackDmg = 30;
	std::cout << "FragTrap: Default constructor is called" << std::endl;
}

FragTrap::~FragTrap()
{
	std::cout << "FragTrap: Destructor is called" << std::endl;
}

void	FragTrap::highFivesGuys(void)
{
	std::cout << "Can we have a high five guys?" << std::endl;
}
