/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 17:43:00 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/17 14:50:52 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Animal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"
#include "WrongAnimal.hpp"
#include "WrongCat.hpp"

int	main(void)
{
	const Animal *zoo[10];

	for (int i = 0; i < 10; ++i)
	{
		if (i % 2)
			zoo[i] = new Dog();
		else
			zoo[i] = new Cat();
	}

	for (int i = 0; i < 10; ++i)
		delete zoo[i];

	std::cout << std::endl;
	std::cout << "Deep copy test" << std::endl;

	const Dog *dog = new Dog();
	const Dog *copy = new Dog(*dog);
	std::string	idea = "Hello World";
	
	dog->getBrain()->setIdea(idea, 0);

	std::cout << &dog->getBrain()->getIdea(0) << " " + dog->getBrain()->getIdea(0) << std::endl;
	std::cout << &copy->getBrain()->getIdea(0) << " " + copy->getBrain()->getIdea(0) << std::endl;

	delete dog;
	delete copy;
	
	std::cout << std::endl;
	std::cout << "Deep copy test" << std::endl;
	const Cat *cat = new Cat();
	const Cat *copy2 = new Cat(*cat);

	delete cat;
	delete copy2;

	return (0);
}
