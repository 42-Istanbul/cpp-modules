/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Dog.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 17:50:32 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/17 14:31:15 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Dog.hpp"

Dog::Dog()
{
	this->_type = "Dog";
	this->_brain = new Brain();
	std::cout << "Dog: default constructor is called" << std::endl;
}

Dog::Dog(const Dog &copyObj) : Animal(copyObj)
{
	this->_type = copyObj._type;
	this->_brain = copyObj._brain;
}

Dog	&Dog::operator = (const Dog &copyObj)
{
	this->_type = copyObj._type;
	this->_brain = copyObj._brain;
	return (*this);
}

Dog::~Dog()
{
	std::cout << "Dog: destructor is called" << std::endl;
}

void	Dog::makeSound(void) const
{
	delete this->_brain;
	std::cout << "Bark!!!" << std::endl;
}

Brain	*Dog::getBrain(void) const
{
	return (this->_brain);
}
