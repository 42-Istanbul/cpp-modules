/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 17:51:54 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/17 14:53:02 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cat.hpp"

Cat::Cat()
{
	this->_type = "Cat";
	this->_brain = new Brain();
	std::cout << "Cat: default constructor called" << std::endl;
}

Cat::Cat(const Cat &copyObj)
{
	*this = copyObj;
	this->_brain = copyObj._brain;
}

Cat	&Cat::operator = (const Cat &copyObj)
{
	this->_type = copyObj._type;
	this->_brain = copyObj._brain;
	return (*this);
}

Cat::~Cat()
{
	std::cout << "Cat: destructor is called" << std::endl;
}

void	Cat::makeSound(void) const
{
	delete this->_brain;
	std::cout << "Meow!!!" << std::endl;
}

Brain	*Cat::getBrain(void) const
{
	return (this->_brain);
}
